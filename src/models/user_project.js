const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const User_project = sequelize.define('User_project', {
  idProject: { type: DataTypes.INTEGER, allowNull: false },
  idUser: { type: DataTypes.INTEGER, allowNull: false },
});

module.exports = User_project;
