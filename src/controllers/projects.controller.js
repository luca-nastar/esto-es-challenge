const {
  selectAll,
  selectByID,
  createProject,
  updateProject,
  deleteProject,
} = require('../services/projects.services');
const { NOT_FOUND, NO_DATA } = require('../utils/errorsConstants');

const getAllProjects = async (req, res) => {
  const { page, search } = req.query;

  try {
    const projects = await selectAll(page, search);

    if (!projects) {
      return res.status(200).json({ ok: false, msg: NO_DATA });
    }

    return res.status(200).json({ ok: true, data: projects });
  } catch (error) {
    return res.status(500).json({ ok: false, msg: error.message });
  }
};

const getProjectByID = async (req, res) => {
  const { id } = req.params;
  try {
    const project = await selectByID(id);

    if (!project) {
      return res.status(200).json({ ok: false, msg: NOT_FOUND });
    }

    return res.status(200).json({ ok: true, data: project });
  } catch (error) {
    return res.status(500).json({ ok: false, msg: error.message });
  }
};

const addProject = async (req, res) => {
  const data = req.body;

  try {
    const project = await createProject(data);

    return res.status(200).json({ ok: true, data: project });
  } catch (error) {
    return res.status(500).json({ ok: false, msg: error.message });
  }
};

const updateProjectbyID = async (req, res) => {
  const { id } = req.params;
  const data = req.body;

  try {
    const isUpdated = await updateProject(id, data);
    return res.status(200).json({ ok: true, isUpdated });
  } catch (error) {
    return res.status(500).json({ ok: false, msg: error.message });
  }
};

const deleteProjectByID = async (req, res) => {
  const { id } = req.params;

  try {
    const isDeleted = await deleteProject(id);
    return res.status(200).json({ ok: true, isDeleted });
  } catch (error) {
    return res.status(500).json({ ok: false, msg: error.message });
  }
};

module.exports = {
  getAllProjects,
  getProjectByID,
  addProject,
  updateProjectbyID,
  deleteProjectByID,
};
