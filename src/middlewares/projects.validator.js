const { body, validationResult } = require('express-validator');
const { NOT_EMPTY } = require('../utils/errorsConstants');

exports.addProjectValidator = [
  body('name').trim().escape().exists().notEmpty().withMessage(NOT_EMPTY),
  body('description')
    .trim()
    .escape()
    .exists()
    .notEmpty()
    .withMessage(NOT_EMPTY),
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ ok: false, errors: errors.array() });
    }

    next();
  },
];
