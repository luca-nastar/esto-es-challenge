const { Router } = require('express');
const {
  getAllProjects,
  getProjectByID,
  addProject,
  deleteProjectByID,
  updateProjectbyID,
} = require('../controllers/projects.controller');
const { addProjectValidator } = require('../middlewares/projects.validator');

const router = Router();

router.get('/projects', getAllProjects);
router.get('/projects/:id', getProjectByID);
router.post('/projects', [addProjectValidator], addProject);
router.put('/projects/:id', updateProjectbyID);
router.delete('/projects/:id', deleteProjectByID);

module.exports = router;
