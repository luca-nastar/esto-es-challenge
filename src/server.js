const express = require('express');
const cors = require('cors');

const app = express();

//Middlewares
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

//Load Routes
const projectsRoutes = require('./routes/projects.route');

//Endpoints
app.use('/api', projectsRoutes);

module.exports = app;
