const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Project = require('./projects');
const User_project = require('./user_project');

const User = sequelize.define('User', {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

User.belongsToMany(Project, { through: User_project, foreignKey: 'idUser' });
Project.belongsToMany(User, { through: User_project, foreignKey: 'idProject' });

module.exports = User;
