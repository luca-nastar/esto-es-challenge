const { Op } = require('sequelize/dist');
const Project = require('../models/projects');
const User = require('../models/users');
const User_project = require('../models/user_project');
const {
  UPDATED_SUCCESS,
  UPDATED_FAILED,
  DELETED_SUCCESS,
  DELETED_FAILED,
} = require('../utils/errorsConstants');

const selectAll = async (page = 1, search = '') => {
  page = page < 1 ? 1 : parseInt(page, 10);
  const limitOf = 10;
  const offsetOf = limitOf * (page - 1);

  try {
    const projects = await Project.findAndCountAll({
      where: { name: { [Op.like]: `%${search}%` }, isActive: true },
      limit: limitOf,
      offset: offsetOf,
    });

    return {
      count: projects.count,
      previousPage: page - 1 > 0 ? page - 1 : page,
      nextPage: offsetOf + limitOf >= projects.count ? page : page + 1,
      data: projects.rows,
    };
  } catch (error) {
    throw error;
  }
};

const selectByID = (id) => Project.findByPk(id);

const createProject = async ({ name, description, assignedUsers = [] }) => {
  const newProject = {
    name: name,
    description: description,
  };

  try {
    const project = await Project.create(newProject);

    if (!assignedUsers) {
      return null;
    }

    assignedUsers = JSON.parse(assignedUsers);
    assignedUsers.map(async (idUser) => {
      await project.addUser(idUser);
    });

    return project;
  } catch (error) {
    throw error;
  }
};

const updateProject = async (id, data) => {
  const { assignedUsers = null } = data;

  const newData = {
    name: data.name,
    description: data?.description,
  };

  try {
    const isUpdated = await Project.update(newData, { where: { id } });

    if (assignedUsers) {
      const project = await Project.findByPk(id);
      await User_project.destroy({ where: { idProject: project.id } });
      await project.addUser(JSON.parse(assignedUsers));
    }

    if (isUpdated[0] === 0) {
      return UPDATED_FAILED;
    }

    return UPDATED_SUCCESS;
  } catch (error) {
    throw error;
  }
};

const deleteProject = async (id) => {
  const isDeleted = await Project.update(
    { isActive: false },
    { where: { id } }
  );

  if (isDeleted[0] === 0) {
    return DELETED_FAILED;
  }

  return DELETED_SUCCESS;
};

module.exports = {
  selectAll,
  selectByID,
  createProject,
  updateProject,
  deleteProject,
};
