const NOT_EMPTY = 'This field cannot be empty.';
const NOT_FOUND = 'Project not found.';
const NO_DATA = 'No data available.';
const UPDATED_SUCCESS = 'The project was updated successfully.';
const UPDATED_FAILED = 'The project was not updated';
const DELETED_SUCCESS = 'The project was deleted successfully.';
const DELETED_FAILED = 'The project was not deleted';

module.exports = {
  NOT_EMPTY,
  NOT_FOUND,
  NO_DATA,
  UPDATED_SUCCESS,
  UPDATED_FAILED,
  DELETED_SUCCESS,
  DELETED_FAILED,
};
